package com.example.finalcalculator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculator.R
import com.example.finalcalculator.Expression as Expression1
import kotlin.Any as Any1


private var Result.Companion.text: String
    get() {
        TODO()
    }
    set(text) {}
















class MainActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        /*Number Buttons*/


        var tvOne = null
        tvOne.setOnClickListener {
            evaluateExpression("1", clear = true)
        }

        var tvTwo = null
        tvTwo.setOnClickListener {
            evaluateExpression("2", clear = true)
        }

        var tvThree = null
        tvThree.setOnClickListener {
            evaluateExpression("3", clear = true)
        }
        var tvFour = null
        tvFour.setOnClickListener {
            evaluateExpression("4", clear = true)
        }

        var tvFive = null
        tvFive.setOnClickListener {
            evaluateExpression("5", clear = true)
        }

        var tvSix = null
        tvSix.setOnClickListener {
            evaluateExpression("6", clear = true)
        }

        var tvSeven = null
        tvSeven.setOnClickListener {
            evaluateExpression("7", clear = true)
        }

        var tvEight = null
        tvEight.setOnClickListener {
            evaluateExpression("8", clear = true)
        }

        var tvNine = null
        tvNine.setOnClickListener {
            evaluateExpression("9", clear = true)
        }

        var tvZero = null
        tvZero.setOnClickListener {
            evaluateExpression("0", clear = true)
        }

        /*Operators*/

        var tvPlus = null
        tvPlus.setOnClickListener {
            evaluateExpression("+", clear = true)
        }

        var tvMinus = null
        tvMinus.setOnClickListener {
            evaluateExpression("-", clear = true)
        }

        var tvMul = null
        tvMul.setOnClickListener {
            evaluateExpression("*", clear = true)
        }

        var tvDivide = null
        tvDivide.setOnClickListener {
            evaluateExpression("/", clear = true)


            var tvClear = null
            tvClear.setOnClickListener {
                var tvExpression = null
                tvExpression!!.text = ""
                var tvResult = null
                tvResult!!.text = ""
            }

            var tvEquals = null
            tvEquals.setOnClickListener {
                var tvExpression = null
                val text = tvExpression!!.text.toString()
                val expression = ExpressionBuilder(text).build()

                val result = expression.evaluate()
                val longResult = result.toLong()
                if (result == longResult.toDouble()) {
                    var tvResult = null
                    tvResult!!.text = longResult.toString()
                } else {
                    var tvResult = null
                    tvResult!!.text = result.toString()
                }
            }



        }
    }

    /*Function to calculate the expressions using expression builder library*/

    fun evaluateExpression(string: String, clear: Boolean) = if(clear) {
        Result.text = ""

    } else {
        {

        }
        Result.text = ""
    }
}

private fun evaluateExpression(s: String, clear: Boolean) {

}

private fun Nothing?.setOnClickListener(function: () -> Unit) {

}

private fun Any1.evaluate() {

}




private fun Any1.toLong() {

}

private fun Any1.toDouble() {

}












